# Proyeto 2 a solución de VPN

## Creacion de la Red Virtual
Para la creacion de la red virtual lo primero que teno que hacer es irno al apartado de "redes virtuales" 
![1](cap1.png "1") 

Luego, le damos a agregar

![1](cap2.png "1") 

Ahora, llenamos los datos y le damos a "Siguiente:direcciones ip"

![1](cap3.png "1") 

Digitamos la ip que deseamos o dejamos la que nos brindan por defecto y agregamos una nueva subred

![1](cap4.png "1") 

Ahora nos saldrá esta ventana la cual llenamos los datos a nuestra preferencia y le damos agregar.

![1](cap5.png "1") 

 Esto lo hacemos para las otras 2 maquinas restantes, como se muestra acontinuación.

 ![1](cap6.png "1") 

 ![1](cap7.png "1") 

 y nos tiene que quedar algo asi

  ![1](cap8.png "1") 

  le damos a "Revisar y crear" y luego crear.
  Y, listo se creó nuestra red virtual

 ![1](cap9.png "1") 




## Creacion de las maquinas virtuales en AZURE


Vamos a entrar a la página portal.azure.com e ingresamos con el correo que nos provee la universidad, en el mismo convenio contamos con un credito de $100 US para poder usar sus recursos. En este caso vamos primero a crear un grupo de recursos que esta dentro del menu de la pagina(las tres rallas horizontales en la esquina superior derecha)



![1](1.png "1") 

Ingresamos y nos va a aparecer una opcion que dice "Agregar" la vamos a seleccionar.

![imagen](2.png "imagen")

En el apartado de grupo de recursos vamos a nombrarlo a nuestro gusto.
Y en region vamos a seleccionar la que menor latencia tengamos. Y listo, precionamos Revisar y crear, segudamente en el boton crear nuevamente.

![imagen](3.png "imagen")

Ahora nuestro grupo de recursos esta creado, entramos a el y dentro hay un boton para crear recursos de color azul vamos a precionarlo.

![imagen](4.png "imagen")

Dentro de el vamos a buscar  la opcion de "Debian Stretch"(Recomendado).

![imagen](5.png "imagen")

y completamos los datos que nos solician:

En tamaño vamos a cambiarlo por un plan mas comodo ya que no vamos a crear un servidor de uso masivo(En nuestro caso seleccionamos el plan B2s de $30 US aproximadamente mensuales)

![imagen](6.png "imagen")

En la clave publica SSH vamos a pegar el contenido de nuestra clave virtual que poseamos en nuestra computadora la cual es importante tener para conectarnos a nuestro servidor.

En el apartado de Puertos de entrada vamos a activar el SSH y HTTP ya que necesitamos el puerto 80 abierto para poder accesar a nuestro webmail desde cualquier parte.

Seguidamente precionamos "Siguiente: Discos >" para continuar nuestra configuracion de la maquina virtual.

En "Tipo de disco del sistema operativo" vamos a seleccionar SSD estandar, ya que no necesitamos gastar de mas para efectos del trabajo

![imagen](7.png "imagen")\


En el apartado de redes vamos a seleccionar la red virtual que creamos para el proyecto correspondiente a la maquina creada.

Y precionamos "Revisar y crear", con esto acabamos con la creacion de la maquina virtual y en "Crear", esperamos de 5 a 10 minutos para que nos crea la maquina en la nuve de Azure.

![imagen](8.png "imagen")

Para accesar a la terminal de nuestra maquina virtual usaremos el siguiente comando:

    ssh -i <ruta de acceso de clave privada> <nombre de usuario>@<ip publica de la maquina virtual Azure> esto en la terminal y listo tendremos acceso a la maquina Azure.


## Creacion y configuracion del OpenVPN

Para la creación y configuración del openVpn lo primero que hay que hacer es conectarse por medio del ssh de mi maquina virtual, despues nos pasamos usuario root, luego de eso digitamos los siguientes comandos

    nano /etc/apt/sources.list

despues guardamos y salimos.
Ahora ejecutamos 

    apt update
y luego, 

    apt upgrade

Luego, ejecutamos

    wget https://git.io/vpn -O openvpn-install.sh

ejecutamos para configurar 

    bash openvpn-install.sh

nos saldrá un tipo cuestionario al cual responderemos:
- en el puerto pondemos el 1194
- en el protocolo elegiremos la opcion UDP
- en el DNS elegiremos Current system resolvers
- en el nombre del cliente pondremos el nombre que gustemos 
 
 ejecutamos para editar 

     nano /etc/openvpn/server.conf

de ahí editamos solo el server y ponemos siguiente

![imagen](cap11.png "imagen")

Ahora copiamos nuestro archivo .ovpn en la direccion /home/openvpn/ que es donde se hospeda mi maquina

    cp client.ovpn /home/openvpn/

luego con la ayuda la aplicacion "winSCP" vamos a sacar nuestro archivo .ovpn
ingresamos nuestra ip(esta ip es la ip publica de nuestra maquina openvpn), el puerto lo dejamos igual, pondremos el nombre nuestra maquina y la contraseña y nos logueamos


![imagen](cap12.png "imagen")

luego, descargamos nuestro archivo .ovpn 

![imagen](cap13.png "imagen")

ahora, en nuestra pc descargamos la aplicacion de openvpn, la instalamos e importamos nuestro archivo .ovpn 

![imagen](cap14.png "imagen")

y nos conectamos 

![imagen](cap15.png "imagen")

nuestro archivo .ovpn queda de esta forma 

![imagen](cap16.png "imagen")

en el está el certificado del cliente, el certificado del CA, la llave TA y la llave del cliente


## Maquina Apache

Creamos la maquina como lo especificamos anteriormente (Debian buster) , luego nos conectamos a ella via ssh.


una vez dentro de ella vamosa a ejecutar lo siguiente para tenerla actualizada: 

    sudo-apt get update

    sudo-apt get upgrade

instalamos apache con el siguiente comando:

    apt-get install apache2

luego habilitamos apache2:

    systemctl enable apache2

También puede verificar el estado de su servicio Apache con el siguiente comando:

    systemctl status apache2

al entrar a la direccion de nuestra ip publica de la maquina podremos ver la pagina de apache is works!.












## Vhost

Ahora procederemos a la instalacion de los vhost que contendran nuestras paginas ejemplo.

Antes de configurar los hosts virtuales, necesitaremos crear los directorios raíz de documentos para nuestros sitios web. Vamos a crearlos en el directorio / var / www / html con los siguientes comandos:

    mkdir -p /var/www/html/'nuestrodominio1'
    mkdir -p /var/www/html/'nuestrodominio2'

en nuestro caso los dominios seran dmail.xyz y rides.xyz

nos vamos al directorio creado 

    cd /var/www/'nuestrodominio1'

y creamos o clonamos el repositorio html que queremos visualizar.

Lo mismo hacemos con el segundo dominio(podemos crear n cantidad de dominios si es necesario).

Ahora hemos creado con éxito las páginas de prueba para ambos dominios. Para que nuestro servidor web Apache pueda acceder a estos archivos, también debemos otorgarles los permisos adecuados y configurar el usuario y el grupo en www-data. Actualizamos los permisos a todo el directorio / var / www / html, con el siguiente comando.

    chown -R www-data: /var/www/html

### Crear los archivos de host virtual


Ahora podemos crear nuestros archivos de host virtuales. Los archivos de configuración del host virtual generalmente terminan con la extensión .conf.
Ejecute el siguiente comando para crear el archivo de configuración de host virtual para nuestro primer dominio, 'nuestrodominio'.xyz:

    nano /etc/apache2/sites-available/'nuestrodominio'.xyz.conf


y dentro del nuevo nano agregamos lo siguiente:

    <VirtualHost *:80>

    ServerAdmin admin@'nuestrodominio'.xyz
    ServerName 'nuestrodominio'.xyz
    ServerAlias www.domain1.com
    DocumentRoot /var/www/html/'ubicacion del dominio'

    ErrorLog ${APACHE_LOG_DIR}/'nuestrodominio'.xyz_error.log
    CustomLog ${APACHE_LOG_DIR}/'nuestrodominio'.xyz_access.log combined

    </VirtualHost>


y hacemos lo mismo con todos los dominios que deseamos agregar.


reiniciamos los servicios apache para que los cambios se apliquen.


si entramos conectados a la vpn ya podremos accesar a los dominios


    ![imagen](10.png "imagen")

## MYSQL

lo primero que haremos es actualizar nuestra maquina 

     sudo apt-get update 
     sudo apt-get upgrade

luego vamos a descargar el paquete 

   wget http://repo.mysql.com/mysql-apt-config_0.8.13-1_all.deb


lo instalamos con el comando

    sudo dpkg -i mysql-apt-config*


procedemos a instalar mysql con 

    sudo apt install default-mysql-server

 luego reiniciamos el servidor con 

    sudo systemctl restart mysql

Ahora vamos  instalacion para la seguridad de nuestro servidor

    sudo mysql_secure_installation

luego no pondra un tipo cuestionario a lo que respondimos a todo si

Ahora podemos ingresamos con

    sudo mysql -u root -p

Y puede crear bases de datos  con:

    create database 'nombrebasedatos';



ahora procedemos a crear unos usuarios

    CREATE USER 'nombre_usuario'@'localhost' IDENTIFIED BY 'tucontrasena';

Y ademas le agregamos privilegios con:

    GRANT ALL PRIVILEGES ON * . * TO 'nombre_usuario'@'localhost';


Ahora refrecamos los privilegios para que se actualicen

    FLUSH PRIVILEGES;

Ahora el usuario puede ingresar normal con el comando y podra ver sus tablas y a las que se le dio acceso:

    mysql -u [nombre de usuario]-p

El usuario podra ver sus bases de datos con:

    show databases;



## Aprovisionamiento de Sakila y World


Primero creamos la carpeta donde vamos a decargar las bases de datos

            mkdir databases


luego lo que vamos a realizar sera el de World 
     
     wget https://downloads.mysql.com/docs/world.sql.gz



Ahora tenemos que descomprimirlo:
     
    gzip -d world.sql.gz



abrimos nuestra base de datos:

    mysql -u root -p

Y ponemos el comando:



 SOURCE /home/mysql/databases/world.sql;


 Para realizar el cambio de base lo que hacemos es:

     USE world;



Y listo tenemos nuestra base de World!

## SHAKILA

decargamos el archivo de sakila con

    wget https://downloads.mysql.com/docs/sakila-db.tar.gz



lo descomprimimos

    tar -xzvf sakila-db.tar.gz

Ahora ingresamos a nuestra base de datos  y realizamos la instalacion

En esta tenemos que agregar los codigos para realizar la instalacion:

    SOURCE /home/mysql/databases/sakila-db/sakila-schema.sql;
    SOURCE /databases/sakila-db/sakila-data.sql;

Y de igual manera realizamos el cambio de base de datos con:

    USE sakila;



Ademas podemos ver las tablas que trae por defecto con:

     SHOW FULL TABLES;





## Instalacion de Phpmyadmin

Realizamos la instalacion con 

    apt-get install phpmyadmin

Al realizar la instalacion nos  pregunta que servidor usar y elegimos apache 


Para finalizar volvemos a reiniciar nuestro servidor 

    sudo service mysql restart
